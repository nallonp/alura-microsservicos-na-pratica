package br.com.alurafood.pagamentos.service;

import br.com.alurafood.pagamentos.dto.ItemDoPedido;
import br.com.alurafood.pagamentos.dto.PagamentoDto;
import br.com.alurafood.pagamentos.http.PedidoClient;
import br.com.alurafood.pagamentos.model.Pagamento;
import br.com.alurafood.pagamentos.model.Status;
import br.com.alurafood.pagamentos.repository.PagamentoRepository;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PagamentoService {

  private final PagamentoRepository pagamentoRepository;
  private final ModelMapper modelMapper;
  private final PedidoClient pedidoClient;

  public Page<PagamentoDto> obterTodos(Pageable paginacao) {
    return pagamentoRepository.findAll(paginacao)
        .map(p -> modelMapper.map(p, PagamentoDto.class));
  }

  public PagamentoDto obterPorId(Long id) {
    Pagamento pagamento = pagamentoRepository.findById(id)
        .orElseThrow(EntityNotFoundException::new);

    PagamentoDto pagamentoDto = modelMapper.map(pagamento, PagamentoDto.class);
    List<ItemDoPedido> itensPedido = pedidoClient.getItensPedido(id).getItens();
    pagamentoDto.setItens(itensPedido);
    return pagamentoDto;
  }

  public PagamentoDto criarPagamento(PagamentoDto pagamentoDto) {
    Pagamento pagamento = modelMapper.map(pagamentoDto, Pagamento.class);
    pagamento.setStatus(Status.CRIADO);
    pagamentoRepository.save(pagamento);
    return modelMapper.map(pagamento, PagamentoDto.class);
  }

  public PagamentoDto atualizarPagamento(Long id, PagamentoDto pagamentoDto) {
    Pagamento pagamento = modelMapper.map(pagamentoDto, Pagamento.class);
    pagamento.setId(id);
    pagamentoRepository.save(pagamento);
    return modelMapper.map(pagamento, PagamentoDto.class);
  }

  public void excluirPagamento(Long id) {
    pagamentoRepository.deleteById(id);
  }

  public void confirmarPagamento(Long id) {
    Optional<Pagamento> pagamento = pagamentoRepository.findById(id);

    if (pagamento.isEmpty()) {
      throw new EntityNotFoundException();
    }

    pagamento.get().setStatus(Status.CONFIRMADO);
    pagamentoRepository.save(pagamento.get());
    pedidoClient.atualizaPagamento(pagamento.get().getPedidoId());
  }

  public void alteraStatus(Long id) {
    Optional<Pagamento> pagamento = pagamentoRepository.findById(id);

    if (pagamento.isEmpty()) {
      throw new EntityNotFoundException();
    }

    pagamento.get().setStatus(Status.CONFIRMADO_SEM_INTEGRACAO);
    pagamentoRepository.save(pagamento.get());
    pedidoClient.atualizaPagamento(pagamento.get().getPedidoId());
  }
}
