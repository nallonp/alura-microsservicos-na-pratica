package br.com.alurafood.pagamentos.dto;

import lombok.Data;

@Data
public class ItemDoPedido {

  private Long id;
  private String descricao;
  private Integer quantidade;

}
