package br.com.alurafood.pagamentos.dto;

import java.util.List;
import lombok.Data;

@Data
public class Pedido {

  private List<ItemDoPedido> itens;
}
