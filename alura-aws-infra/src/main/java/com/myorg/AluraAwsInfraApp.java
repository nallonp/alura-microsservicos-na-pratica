package com.myorg;

import software.amazon.awscdk.App;

public class AluraAwsInfraApp {
    public static void main(final String[] args) {
        App app = new App();

        AluraVpcStack vpcStack = new AluraVpcStack(app, "VPC");
        AluraClusterStack clusterStack = new AluraClusterStack(app, "CLUSTER", vpcStack.getVpc());
        clusterStack.addDependency(vpcStack);
        AluraRdsStack aluraRdsStack = new AluraRdsStack(app, "RDS", vpcStack.getVpc());
        aluraRdsStack.addDependency(vpcStack);
        AluraServiceStack aluraServiceStack = new AluraServiceStack(app, "SERVICE", clusterStack.getCluster());
        aluraServiceStack.addDependency(clusterStack);
        aluraServiceStack.addDependency(aluraRdsStack);
        app.synth();
    }
}

