package com.myorg;

import software.amazon.awscdk.Duration;
import software.amazon.awscdk.Fn;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.StackProps;
import software.amazon.awscdk.services.applicationautoscaling.EnableScalingProps;
import software.amazon.awscdk.services.ecr.IRepository;
import software.amazon.awscdk.services.ecr.Repository;
import software.amazon.awscdk.services.ecs.*;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions;
import software.constructs.Construct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AluraServiceStack extends Stack {
    public AluraServiceStack(final Construct scope, final String id, final Cluster cluster) {
        this(scope, id, null, cluster);
    }

    public AluraServiceStack(final Construct scope, final String id, final StackProps props, final Cluster cluster) {
        super(scope, id, props);

        Map<String, String> autenticacao = new HashMap<>();
        autenticacao.put("DB_URL", "jdbc:postgresql://" + Fn.importValue("pedidos-db-endpoint") + ":5432/postgres");
        autenticacao.put("DB_USER", "admin93");
        autenticacao.put("DB_PASSWORD", Fn.importValue("pedidos-db-senha"));

        IRepository myEcrRepo = Repository.fromRepositoryName(this, "repositorio", "my-ecr-repo");

        ApplicationLoadBalancedFargateService loadBalancedFargateService = ApplicationLoadBalancedFargateService.Builder.create(this, "AluraService")
                .serviceName("alura-service-ola")
                .cluster(cluster)           // Required
                .cpu(512)                   // Default is 256
                .desiredCount(1)            // Default is 1
                .listenerPort(8080)
                .assignPublicIp(true)
                .taskImageOptions(
                        ApplicationLoadBalancedTaskImageOptions.builder()
                                //.image(ContainerImage.fromRegistry("nallon/api-pedidos"))
                                .image(ContainerImage.fromEcrRepository(myEcrRepo))
                                .containerPort(8080)
                                .containerName("app_ola")
                                .environment(autenticacao)
                                .build())
                .healthCheck(HealthCheck.builder()
                        .command(List.of("CMD-SHELL", "curl -f http://localhost:8080/pedidos/ || exit 1"))
                        .timeout(Duration.seconds(5))
                        .interval(Duration.seconds(10))
                        .retries(10)
                        .startPeriod(Duration.minutes(1))
                        .build())
                .memoryLimitMiB(1024)       // Default is 512
                .publicLoadBalancer(true)   // Default is true
                .build();

        ScalableTaskCount scalableTarget = loadBalancedFargateService.getService().autoScaleTaskCount(EnableScalingProps.builder()
                .minCapacity(1)
                .maxCapacity(20)
                .build());
        scalableTarget.scaleOnCpuUtilization("CpuScaling", CpuUtilizationScalingProps.builder()
                .targetUtilizationPercent(50)
                .build());
        scalableTarget.scaleOnMemoryUtilization("MemoryScaling", MemoryUtilizationScalingProps.builder()
                .targetUtilizationPercent(50)
                .build());
    }
}
