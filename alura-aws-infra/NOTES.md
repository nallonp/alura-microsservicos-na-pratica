# Links

### Set-up CDK cli:
- https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html
### Creating an AWS Fargate service using CDK:
- https://docs.aws.amazon.com/pt_br/cdk/v2/guide/ecs_example.html

# Comandos:

- cdk ls - Lists all stacks in the app
- cdk deploy - Deploys the stack(s) named STACKS into your AWS account
- cdk destroy - Destroy the stack(s) named STACKS
- cdk deploy --parameters Rds:senha=12345678 Rds - Deploy com parâmetros.

# Bizus
## Redo bootstrap:
```
aws cloudformation delete-stack --stack-name CDKToolkit
cdk bootstrap
```
# ECR
## Tagging and pushing your docker images to ECR.
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password - stdin 127410334518.dkr.ecr.us-east-1.amazonaws.com
docker tag img-pedidos-ms: latest 127410334518.dkr.ecr.us-east-1.amazonaws.com/img-pedidos-ms:latest`
```
