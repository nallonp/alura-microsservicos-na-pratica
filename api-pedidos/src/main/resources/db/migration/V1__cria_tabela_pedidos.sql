CREATE TABLE alura_pedidos.pedidos
(
    id        bigint primary key generated always as identity,
    data_hora timestamp NOT NULL,
    status    varchar   NOT NULL
)