CREATE TABLE alura_pedidos.item_do_pedido
(
    id         bigint primary key generated always as identity,
    descricao  varchar DEFAULT NULL,
    quantidade int    NOT NULL,
    pedido_id  bigint NOT NULL,
    FOREIGN KEY (pedido_id) REFERENCES pedidos (id)
)