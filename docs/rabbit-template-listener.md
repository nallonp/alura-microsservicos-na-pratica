RabbitTemplate

Para fazer o envio das mensagens para o RabbitMQ, ao realizar uma requisição do tipo POST em nosso endpoint, utilizamos o RabbitTemplate. Mas, afinal, que classe é essa que injetamos no nosso controller?

O RabbitTemplate é uma classe helper que simplifica o processo de envio e recebimento de mensagens para o RabbitMQ. Ela tem vários métodos para que se possa personalizar como vai ser a conversão das mensagens, a routing key ou exchange padrão para envio, além de algumas configurações de resposta, quando se deseja a confirmação de consumo da mensagem.

Para saber mais sobre essa classe, acesse a [documentação do RabbitTemplate](https://docs.spring.io/spring-amqp/api/org/springframework/amqp/rabbit/core/RabbitTemplate.html).

RabbitListener

Para consumir mensagens de uma fila, a dependência do spring-boot-starter-amqp disponibiliza a anotação @RabbitListener, que recebe como parâmetro um array de Strings correspondentes aos nomes das filas que serão consumidas. Dessa forma, assim que a aplicação é inicializada, o método com essa anotação começará a ser executado e as mensagens da(s) fila(s) serão consumida(s).

O método geralmente recebe como parâmetro um tipo Message. A anotação @Payload indica que o parâmetro do método vai receber o corpo da mensagem. Essa anotação é opcional quando há apenas um parâmetro no método. Mais informações podem ser visualizadas na [documentação do RabbitListener](https://docs.spring.io/spring-amqp/api/org/springframework/amqp/rabbit/annotation/RabbitListener.html).

Shovel

Plugin para encaminhar as mensagens de uma fila para outra.

``
rabbitmq-plugins enable rabbitmq_shovel rabbitmq_shovel_management``