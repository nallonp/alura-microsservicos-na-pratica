package com.alura.avaliacao.model;

public enum Status {
    CRIADO, CONFIRMADO, CANCELADO, CONFIRMADO_SEM_INTEGRACAO
}
