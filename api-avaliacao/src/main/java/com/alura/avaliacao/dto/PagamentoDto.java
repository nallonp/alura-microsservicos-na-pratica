package com.alura.avaliacao.dto;

import com.alura.avaliacao.model.Status;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PagamentoDto {

    private Long id;
    private BigDecimal valor;
    private String nome;
    private String numero;
    private String expiracao;
    private String codigo;
    private Status status;
    private Long pedidoId;
    private Long formaDePagamentoId;
    private List<ItemDoPedido> itens;
}
