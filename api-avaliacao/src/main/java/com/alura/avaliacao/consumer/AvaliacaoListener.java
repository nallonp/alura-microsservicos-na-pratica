package com.alura.avaliacao.consumer;

import com.alura.avaliacao.dto.PagamentoDto;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class AvaliacaoListener {

    @RabbitListener(queues = "pagamentos.detalhes-avaliacao")
    public void recebeMensagem(PagamentoDto pagamentoDto) {
        System.out.println(pagamentoDto.getId());
        System.out.println(pagamentoDto.getNumero());

        if (pagamentoDto.getNumero().equals("0000")) {
            throw new RuntimeException("não consegui processar");
        }
        String mensagem = """
                \nDados do pagamento: %s
                Número do pedido: %s
                Valor: R$ %s
                Status: %s
                """.formatted(pagamentoDto.getId(), pagamentoDto.getPedidoId(), pagamentoDto.getValor(), pagamentoDto.getStatus());
        System.out.println("- AVALIAR PEDIDO - " + mensagem);
    }
}
